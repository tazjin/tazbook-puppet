import qualified Data.Map                     as M
import           Graphics.X11.ExtraTypes.XF86
import           System.Exit
import           XMonad
import           XMonad.Config.Desktop
import           XMonad.Hooks.DynamicLog
import           XMonad.Hooks.ManageDocks
import           XMonad.Hooks.SetWMName
import           XMonad.Layout.Maximize
import           XMonad.Layout.NoBorders
import qualified XMonad.StackSet              as W
import           XMonad.Util.Run

-- Custom functions for keybindings etc.
screenshotRegion, screenshotFullscreen :: X ()
screenshotRegion = spawn "xfce4-screenshooter -r -s ~/Screenshots/"
screenshotFullscreen = spawn "xfce4-screenshooter -f -s ~/Screenshots/"

-- Screen lock function
lockScreen :: X ()
lockScreen = spawn "alock -auth md5:hash=0d107d09f5bbe40cade3de5c71e9e9b7"

-- Entire list of keybindings

-- Entire list of keybindings
myKeys conf@(XConfig {XMonad.modMask = modMask}) = M.fromList $
    -- launch a terminal
    [ ((modMask,		xK_Return), spawn $ XMonad.terminal conf)
    -- launch dmenu
    , ((modMask,               xK_p     ), spawn "exe=`dmenu_path | dmenu` && eval \"exec $exe\"")
    -- close focused window
    , ((modMask .|. mod4Mask, xK_c     ), kill)
     -- Rotate through the available layout algorithms
    , ((modMask,               xK_space ), sendMessage NextLayout)
    --  Reset the layouts on the current workspace to default
    , ((modMask .|. mod4Mask, xK_space ), setLayout $ XMonad.layoutHook conf)
    -- Resize viewed windows to the correct size
    , ((modMask,               xK_n     ), refresh)
    -- Move focus to the next window
    , ((modMask,               xK_Tab   ), windows W.focusDown)
    -- Move focus to the next window
    , ((modMask,               xK_k     ), windows W.focusDown)
    -- Move focus to the previous window
    , ((modMask,               xK_j     ), windows W.focusUp  )
    -- Move focus to the master window
    , ((modMask .|. mod4Mask, xK_m     ), windows W.focusMaster  )
    -- Maximize the focused window temporarily
    , ((modMask,               xK_m     ), withFocused $ sendMessage . maximizeRestore)
    -- Swap the focused window and the master window
    , ((modMask .|. mod4Mask, xK_Return), windows W.swapMaster)
    -- Swap the focused window with the next window
    , ((modMask .|. mod4Mask, xK_j     ), windows W.swapDown  )
    -- Swap the focused window with the previous window
    , ((modMask .|. mod4Mask, xK_k     ), windows W.swapUp    )
    -- Shrink the master area
    , ((modMask,               xK_h     ), sendMessage Shrink)
    -- Expand the master area
    , ((modMask,               xK_l     ), sendMessage Expand)
    -- Push window back into tiling
    , ((modMask,               xK_t     ), withFocused $ windows . W.sink)
    -- Increment the number of windows in the master area
    , ((modMask              , xK_comma ), sendMessage (IncMasterN 1))
    -- Decrement the number of windows in the master area
    , ((modMask              , xK_period), sendMessage (IncMasterN (-1)))
    -- Screenshot of whole screen
    , ((modMask, xK_f), screenshotFullscreen)
    -- Screenshot of screen region
    , ((modMask .|. mod4Mask, xK_f), screenshotRegion)
    -- Lock screen
    , ((modMask .|. mod4Mask, xK_l), lockScreen)
    -- Quit xmonad
    , ((modMask .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))
    -- Restart xmonad
    , ((modMask              , xK_q     ), restart "xmonad" True)
    -- to hide/unhide the panel
    , ((modMask              , xK_b), sendMessage ToggleStruts)
    -- Brightness lizard
    , ((0, xF86XK_MonBrightnessUp), spawn "xbacklight +15")
    , ((0, xF86XK_MonBrightnessDown), spawn "xbacklight -15")
    -- Audio lizard
    , ((0, xF86XK_AudioMute), spawn "pulseaudio-ctl mute")
    , ((0, xF86XK_AudioRaiseVolume), spawn "pulseaudio-ctl up")
    , ((0, xF86XK_AudioLowerVolume), spawn "pulseaudio-ctl down")
    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modMask, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, mod4Mask)]]
    ++

    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. modMask, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_e, xK_w, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, mod4Mask)]]

-- Use this stuff!
main = do
  xmproc <- spawnPipe "xmobar"
  xmonad desktopConfig
       { modMask = mod3Mask
       , focusedBorderColor = "#ff9500" -- iOS 7 colours orange
       , startupHook = setWMName "LG3D"
       , terminal = "xfce4-terminal"
       , keys = myKeys
       , layoutHook = layoutHook desktopConfig ||| noBorders Full
       , logHook = dynamicLogWithPP xmobarPP
                   { ppOutput = hPutStrLn xmproc
                   , ppTitle = xmobarColor "green" "" . shorten 50}
       }
