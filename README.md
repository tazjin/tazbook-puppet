This repository contains the Puppet configuration for my main machine.

A couple of things are pulled in from a second (private) repository that contains things such as SSH keys, so this repository will not be applicable without further modifications.

Some of the configuration might be of interest to users of the Thinkpad T440s, such as the fprint module for fingerprint reading support.
