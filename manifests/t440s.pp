# Umbrella class for all T440s configuration

class tazbook_defaults::t440s {
  include tazbook_defaults::t440s::audio
  include tazbook_defaults::t440s::broadband
  include tazbook_defaults::t440s::generic
  include tazbook_defaults::t440s::touchpad
}
