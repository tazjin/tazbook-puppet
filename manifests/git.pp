# Set up git tools and config
class tazbook_defaults::git {
  $git_pkgs = ['git', 'tig', 'hub']

  package { $git_pkgs:
    ensure => installed,
  }

  file {"${tazbook_defaults::homedir}/.gitconfig":
    ensure => present,
    source => 'puppet:///modules/tazbook_defaults/dotfiles/gitconfig',
  }
}
