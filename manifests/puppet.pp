# Meta module

class tazbook_defaults::puppet {
  package {'puppet':
    ensure => latest,
  }

  exec {'install_vcsrepo_module':
    path    => '/usr/bin',
    command => 'puppet module install puppetlabs/vcsrepo',
    creates => '/etc/puppet/modules/vcsrepo',
    require => Package['puppet'],
  }

  File {
    owner  => 'root',
    group  => 'root',
  }

  file {'/etc/puppet/modules':
    owner   => $tazbook_defaults::username,
    group   => 'users',
    recurse => true,
  }

  file {'/etc/puppet/manifests':
    ensure => directory,
  }

  file {'/etc/puppet/manifests/init.pp':
    ensure => present,
    source => 'puppet:///modules/tazbook_defaults/init.pp',
  }

  file {'/etc/hiera.yaml':
    ensure => present,
    source => 'puppet:///modules/tazbook_defaults/hiera.yaml',
    mode   => '0644',
  }

  file {'/etc/puppet/hiera.yaml':
    ensure => link,
    target => '/etc/hiera.yaml',
  }

  file {'/etc/puppet/hieradata':
    ensure => directory,
    owner  => $tazbook_defaults::username,
  }
}
