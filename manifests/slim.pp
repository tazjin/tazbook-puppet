# Install slim (login manager)

class tazbook_defaults::slim {
  $packages = ['slim', 'slim-themes', 'archlinux-themes-slim']

  package { $packages:
    ensure => installed,
    before => File['/etc/slim.conf'],
  }

  file { '/etc/slim.conf':
    ensure => present,
    source => 'puppet:///modules/tazbook_defaults/slim.conf',
  }

  service {'slim':
    ensure => running,
    enable => true,
  }
}
