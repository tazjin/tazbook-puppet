# Initialization class

class tazbook_defaults ($username = 'vincent') {
  $homedir = "/home/${username}"

  # Required things go first
  require tazbook_defaults::puppet
  require tazbook_defaults::pacman

  # Stuff that is an absolute must
  include tazbook_defaults::emacs
  include tazbook_defaults::git
  include tazbook_defaults::software
  include tazbook_defaults::aur

  # X related
  include tazbook_defaults::slim
  include tazbook_defaults::i3wm
  include tazbook_defaults::x11
}
