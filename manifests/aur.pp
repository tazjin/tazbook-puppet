# TODO: After yaourt, we don't have an easy way to install AUR packages here.
# Currently I will just alert with the package names that need to be installed.

class tazbook_defaults::aur {
  $aur_pkgs = ['ttf-mac-fonts', 'thinkpad-t440s-brightness-keys', 'compton',
               'numix-icon-theme-git', 'numix-circle-icon-theme-git',
               'systemd-emacs-daemon']

  notify {"Following AUR packages need to be installed: ${aur_pkgs}":}
}
