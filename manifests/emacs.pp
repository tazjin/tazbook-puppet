# Installs emacs, sets up my emacs configuration and
# installs and activates the Emacs daemon

class tazbook_defaults::emacs {
  # Needs SSH to be configured to have SSH keys etc.
  require tazbook_secrets

  $emacsdir = "${tazbook_defaults::homedir}/.emacs.d"

  package {'emacs':
    ensure   => latest,
    provider => 'pacman',
  }

  vcsrepo {$emacsdir:
    ensure   => present,
    source   => 'git@github.com:tazjin/emacs.d.git',
    provider => git,
    user     => $tazbook_defaults::username,
  }

  service {'emacs@vincent.service':
    ensure   => running,
    enable   => true,
    provider => 'systemd',
  }
}
