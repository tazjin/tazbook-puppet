# Pacman configuration

class tazbook_defaults::pacman {
  file {'/etc/pacman.conf':
    ensure => present,
    owner  => root,
    group  => root,
    mode   => '0644',
    source => 'puppet:///modules/tazbook_defaults/pacman.conf',
  }
}
