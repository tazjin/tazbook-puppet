# Proper software that I use
class tazbook_defaults::software {
  $installed_software = ['tree', 'the_silver_searcher', 'dnsutils',
                         'gnome-keyring', 'pass']
  $latest_software = ['vlc', 'skype']

  package { $installed_software:
    ensure => installed,
  }

  package { $latest_software:
    ensure => latest,
  }

  file {"${tazbook_defaults::homedir}/bin":
    ensure => directory,
    owner  => $tazbook_defaults::username,
  }
}
