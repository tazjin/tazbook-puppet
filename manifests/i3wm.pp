# Installs the i3 window manager, related tools and configuration

class tazbook_defaults::i3wm {
  $i3wm_pkgs = ['i3-wm', 'i3lock', 'i3status', 'xfce4-screenshooter',
                'numix-themes', 'xfce4-terminal']

  $config_dirs = ["${tazbook_defaults::homedir}/.config/i3",
                  "${tazbook_defaults::homedir}/.config/i3status"]

  # Install packages
  package { $i3wm_pkgs:
    ensure   => latest,
    provider => 'pacman'
  }

  file { $config_dirs:
    ensure => directory,
    owner  => $tazbook_defaults::username
  }

  # Add configuration
  file {"${tazbook_defaults::homedir}/.config/i3status/config":
    ensure => present,
    owner  => $tazbook_defaults::username,
    source => 'puppet:///modules/tazbook_defaults/dotfiles/i3status.config',
  }

  file {"${tazbook_defaults::homedir}/.config/i3/config":
    ensure => present,
    owner  => $tazbook_defaults::username,
    source => 'puppet:///modules/tazbook_defaults/dotfiles/i3.config',
  }
}
