# Installs X-related configuration

class tazbook_defaults::x11 {
  $x11_pkgs = ['lxappearance', 'xorg-xinit', 'xorg-xmodmap', 'xorg-xbacklight']

  file {'/etc/X11/xorg.conf.d/60-layout.conf':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/tazbook_defaults/60-layout.conf',
  }

  file {"${tazbook_defaults::homedir}/.xinitrc":
    ensure => present,
    owner  => $tazbook_defaults::username,
    mode   => '0744',
    source => 'puppet:///modules/tazbook_defaults/dotfiles/xinitrc',
  }

  file {"${tazbook_defaults::homedir}/.Xmodmap":
    ensure => present,
    owner  => $tazbook_defaults::username,
    source => 'puppet:///modules/tazbook_defaults/dotfiles/Xmodmap',
  }

  package { $x11_pkgs:
    ensure   => latest,
    provider => 'pacman',
  }

}
