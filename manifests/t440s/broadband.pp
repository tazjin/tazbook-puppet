# Broadband works pretty much OOB, just need
# ModemManager installed.

# Currently NetworkManager asks for the PIN on
# every "unsuspend"/boot of the system (because
# the Ericsson device can't identify the SIM).
# I never switch SIMs so maybe it's hardcodeable?

class tazbook_defaults::t440s::broadband {

  # APN configuration was done manually in
  # NetworkManager. I need to puppetize it
  # but it probably wouldn't be interesting
  # to other people because it's Telia (Sweden)
  # specific.

  package { 'modemmanager':
    ensure => installed,
  }
}
