# Generic drivers I installed that worked OOB

class tazbook_defaults::t440s::generic {
  $t440s_pkgs = ['xf86-video-intel', 'xorg-xrandr']

  package { $t440s_pkgs:
    ensure => installed,
  }

  # Fix brightness control buttons
  file { '/etc/tmpfiles.d/t440s-brightness.conf':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/tazbook_defaults/t440s/brightness',
  }

  # Use udev to trigger xrandr on display hotplug
  file { '/etc/udev/rules.d/55-xrandr.rules':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/tazbook_defaults/t440s/55-xrandr.rules',
  }

  file { '/usr/bin/screen_change':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    source => 'puppet:///modules/tazbook_defaults/t440s/screen_change',
  }
}
