# Configuration specific to the touchpad

# TODO: Middle-button scrolling currently
# broken

class tazbook_defaults::t440s::touchpad {
  
  package {'xf86-input-synaptics':
    ensure => installed,
  }

  package {'libevdev':
    ensure => installed,
  }

  file {'/etc/X11/xorg.conf.d/95-synaptics.conf':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/tazbook_defaults/t440s/95-synaptics.conf',
  }
}
