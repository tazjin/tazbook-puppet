# Adds audio configuration

# TODO: Pulseaudio sometimes doesn't unmute headphones 
# or speakers when switching output device (master audio
# is set properly)

class tazbook_defaults::t440s::audio {
  # Do you remember the times of having to struggle
  # with pulseaudio? Now it just works!
  package { ['paprefs', 'pulseaudio']:
    ensure => installed,
  }

  # This file enables the appropriate alsa drivers

  file {'/etc/modprobe.d/alsa.conf':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    source => 'puppet:///modules/tazbook_defaults/t440s/alsa.conf',
  }

}
